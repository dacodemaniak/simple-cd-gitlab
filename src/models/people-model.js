export class PeopleModel {
    #lastname = ''
    #firstname = ''
    #band = ''

    get lastname() {
        return this.#lastname
    }

    get firstname() {
        return this.#firstname
    }

    get band() {
        return this.#band
    }

    set lastname(lastname) {
        this.#lastname = lastname
    }

    set firstname(firstname) {
        this.#firstname = firstname
    }

    set band(band) {
        this.#band = band
    }
}