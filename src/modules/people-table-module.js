export class PeopleTableModule {
    #peoples = []

    /**
     * PeopleService
     */
    #service = null

    constructor() {
        this.#service = new PeopleService()
        this.#peoples = this.#service.peoples

        this.#buildTable()
    }

    get peoples() {
        return this.#peoples
    }

    #buildTable() {
        // Loop over people and feed rows
        const tbody = document.querySelector('tbody')

        for (const people of this.#peoples) {
            // Create a row for each people
            const tr = document.createElement('tr')

            // Add a td for the future checkbox using a static method
            tr.appendChild(Main.#makeCheckboxTd())

            // Try to traverse the People object
            for (const attribute in people) {
                const td = document.createElement('td')
                td.innerText = people[attribute]
                tr.appendChild(td)
            }

            // Append tr to tbody
            tbody.appendChild(tr)
        }
    }

    static #makeCheckboxTd() {
        const td = document.createElement('td')
        const templateLoader = new TemplateLoader('item-checkbox')
        try {
            const checkbox = templateLoader.loadTemplate()
            td.appendChild(checkbox)
        } catch(e) {
            td.innerHTML = '&nbsp;'
        }
        
        return td
    }
}

// Event listener for the main checkbox
document.getElementById('main-checkbox').addEventListener(
    'click',
    (event) => {
        const checkbox = event.target

        // Need to get all item-cheboxes
        const itemCheckboxes = document.getElementsByClassName('item-checkbox')
        
        let doCheck = false
        
        if (checkbox.checked) {
            doCheck = true
        }

        for (const itemCheckbox of itemCheckboxes) {
            itemCheckbox.checked = doCheck
        }
    }
)

const tbody = document.querySelector('tbody')
tbody.addEventListener(
    'click',
    (event) => {
        if (event.target.tagName === 'INPUT') {
            const checkbox = event.target
            // Ensure is the good checkbox
            if (checkbox.classList.contains('item-checkbox')) {
                const mainCheckbox = document.getElementById('main-checkbox')
                if (checkbox.checked === false) {
                    mainCheckbox.checked = false
                } else {
                    const itemCheckboxes = Array.from(document.getElementsByClassName('item-checkbox'))
                    const checkedItems = itemCheckboxes.filter((itemCheckbox) => itemCheckbox.checked)

                    mainCheckbox.checked = !(checkedItems.length - app.peoples.length)
                }
            }
        }
    }
)