import { TemplateLoader } from "./_helpers/template-loader"
import { PeopleTableModule } from "./modules/people-table-module"
import { PeopleService } from "./services/people-service"
class Main {
    constructor() {
        this.#run()
    }

    #run() {
        const title = document.querySelector('h1')
        title.innerText = 'JS'

        // Make an instance of PeopleTable
        const peopleTable = new PeopleTableModule()
    }
}

// Self callable function to run the Main class
let app
(function () {
    app = new Main()
})()