/**
 * @jest-environment jsdom
 */

import { PeopleModel } from '../src/models/people-model'

describe(`PeopleModel`, () => {
    test('Two object shared same memory space', () => {
        const morgane = new PeopleModel()
        morgane.lastname = 'Lafée'
        morgane.firstname = 'Morgane'
        morgane.band = 'La flûte enchantée'

        const jeanluc = morgane
        jeanluc.firstname = 'Jean-Luc'
        jeanluc.lastname = 'Aubert'
        jeanluc.band = 'Elmer Foodbit'

        const xavier = {... jeanluc}
        xavier.lastname = 'Rolland'
        xavier.firstname = 'Xavier'
        xavier.band = 'Aélion'

        expect(morgane.firstname).toBe('Jean-Luc')
        expect(xavier.firstname).toBe('Xavier')
    })
})